// pages/Commpents/Tabbar/tabbar.js
Page({
    /**
     * 页面的初始数据
     */
    data: {
        active: null,
        page_list: [
            {
                "url": "/pages/Game/room_hall/room_hall",
                "titleName":"大厅房间列表 - ausheng",
            }, {
                "url": "/pages/Friends/friend_list/friend_list",
                "titleName":"好友列表 - ausheng",
            }, {
                "url": "/pages/Message/message_list/message_list",
                "titleName":"消息列表 - ausheng",
            }, {
                "url": "/pages/System/system_info/system_info",
                "titleName":"个人设置 - ausheng",
            }
        ]
    },
    onChange(event) {
        
        if(event.detail !== this.data.active){
            //修改标题
            wx.setNavigationBarTitle({
                title: this.data.page_list[event.detail].titleName
            })
            //切换页面
            wx.switchTab({ 
                url: this.data.page_list[event.detail].url
            });
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {},

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {},

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {},

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {}
})