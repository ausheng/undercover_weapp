//封装请求的方法,
import Notify from '../miniprogram_npm/@vant/weapp/notify/notify';
import Toast  from '../miniprogram_npm/@vant/weapp/toast/toast';

export function getUrlRules(url) {
	var url_rules = [
					'/game/player/login',				//玩家登陆
					'/game/player/register',			//玩家注册
					'/game/player/sendtelphonecode',	//获取验证码
				];
	for (var i in url_rules) {
	    if (url_rules[i] == url) { return true }
    }
	return false;
}

export function getPrefix() {
    console.log(__wxConfig.envVersion)
    if (__wxConfig.envVersion == "develop") {
        return "http://api.ivsheng.com";//http://api.yanxusheng.me
    } else {    //develop trial  release 开发、体验、正式
        return "http://api.ivsheng.com";
    }
}

export function GET(url, params = {}) {
    if (this.$getCookie('user_token') == null && url != '/admin/login/login') {
        this.$notify({ type: 'danger', message: '页面失效，请重新登录！'});
        return false
    }
    
	var res_headers = {};
    res_headers.token = this.$getCookie('user_token') ? this.$getCookie('user_token') : '';
    res_headers['Content-Type'] = 'application/json;charset=UTF-8';
    
    url = this.$url + url
    for (var i in params) {
        if (params[i] == "" && params[i] !== 0) {
            delete params[i];
        }
    }
    return new Promise((resolve, reject) => {
        this.$axios.get(url, {
            headers: res_headers,
            params: params,
            timeout: 30 * 1000
        }).then(response => {
            resolve(response.data)
        }).catch(err => {
			console.log(err);
			this.$notify({ type: 'danger', message: err});
            reject(err)
        })
    })
}

export function POST(url, params = {}) {
    if (!wx.getStorageSync('player_token') && !getUrlRules(url)) {
		Notify({ type: 'danger', message: '页面失效，请重新登录！'});
        return false;
    }
    Toast.loading({ duration: 0,  message: '加载中...', forbidClick: true });
    wx.showNavigationBarLoading();

	var res_headers = {};
    res_headers['player_token'] = wx.getStorageSync('user_token') ? wx.getStorageSync('user_token') : '';
    if (url.indexOf('uploadFiles') >= 0) {
        res_headers['Content-Type'] = 'multipart/form-data';
    } else {
		res_headers['Content-Type'] = 'application/json;charset=UTF-8';
    }

    
    // for (var i in params) {
    //     if (params[i] == "" && params[i] !== 0) { delete params[i]; }
    // }
    let send_params = {};
    for (var i in params) {
        if ( params[i] !== "" ) { send_params[i] = params[i]; }
    }

    return new Promise((resolve, reject) => {
        wx.request({
            url : getPrefix() + url,
            data: send_params,
            header : res_headers,
            timeout: 3000,
            method : "POST",
            success: response => { resolve(response.data) },
            fail : err => {
			    Notify({ type: 'danger', message: '服务器异常'+url});
                reject(err)
            },
            complete : response => {
                wx.hideNavigationBarLoading();
                Toast.clear();
            }
        })
    })
}
