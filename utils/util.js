const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : `0${n}`
}

//时间戳转换成日期时间
function timeToData(unixtime) {
    var dateTime = new Date(parseInt(unixtime))
    var year     = dateTime.getFullYear();
    var month    = dateTime.getMonth() + 1;
    var day      = dateTime.getDate();
    var hour     = dateTime.getHours();
    var minute   = dateTime.getMinutes();
    var second   = dateTime.getSeconds();
    var now      = new Date();
    var now_new  = Date.parse(now.toDateString());  //typescript转换写法
    var milliseconds = now_new - dateTime;
    var timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    return timeSpanStr;
}
// 根据数组中的对象的属性数值大小进行排序（从小到大）
function sortNumber(property) {
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        return value1 - value2;
    }
}
// 验证手机号码是否正确并且得出是哪一家运行商
function regNumber(mobileNo) {
    //移动：134(0 - 8) 、135、136、137、138、139、147、150、151、152、157、158、159、178、182、183、184、187、188、198 
    //联通：130、131、132、145、155、156、175、176、185、186、166
    //电信：133、153、173、177、180、181、189、199 
    var move = /^((134)|(135)|(136)|(137)|(138)|(139)|(147)|(150)|(151)|(152)|(157)|(158)|(159)|(178)|(182)|(183)|(184)|(187)|(188)|(198))\d{8}$/g;
    var link = /^((130)|(131)|(132)|(155)|(156)|(145)|(185)|(186)|(176)|(175)|(170)|(171)|(166))\d{8}$/g;
    var telecom = /^((133)|(153)|(173)|(177)|(180)|(181)|(189)|(199))\d{8}$/g;
    if (move.test(mobileNo)) {
        return 'move';
    } else if (link.test(mobileNo)) {
        return 'link';
    } else if (telecom.test(mobileNo)) {
        return 'telecom';
    } else {
        return false;
    }
}
//验证身份证格式
const identityCodeValid = sId => {
    const aCity = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" };
    var iSum = 0;
    var info = "";

    if (!/^\d{17}(\d|X|x)$/i.test(sId)) return false;
    sId = sId.replace(/x$/i, "a");
    if (aCity[parseInt(sId.substr(0, 2))] == null) return false;

    var sBirthday = sId.substr(6, 4) + "-" + Number(sId.substr(10, 2)) + "-" + Number(sId.substr(12, 2));
    var d = new Date(sBirthday.replace(/-/g, "/"));
    
    if (sBirthday != (d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate())) return false;

    for (var i = 17; i >= 0; i--) iSum += (Math.pow(2, i) % 11) * parseInt(sId.charAt(17 - i), 11);
    
    if (iSum % 11 != 1) return false;
    //aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");//此次还可以判断出输入的身份证号的人性别
    return true;
}
// url参数解析
function getUrlkey(url) {
    var params = {};
    var urls = url.split("?");

    if (urls[1]) {
        var arr = urls[1].split("&");
        for (var i = 0, l = arr.length; i < l; i++) {
            var a = arr[i].split("=");
            params[a[0]] = a[1];
        }
        return params;
    } else {
        return urls[0]
    }
}
// 获取小数点后两位
function returnFloat(value) {
    var value = Math.round(parseFloat(value) * 100) / 100;
    var xsd = value.toString().split(".");

    if (xsd.length == 1) {
        value = value.toString() + ".00";
        return value;
    } else if (xsd.length > 1) {
        if (xsd[1].length < 2) {
            value = value.toString() + "0";
        }
        return value;
    }
}
module.exports = {
    request,
    timeToData,
    sortNumber,
    regNumber, 
    getUrlkey, 
    returnFloat, 
    identityCodeValid
};
