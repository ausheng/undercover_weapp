// pages/Game/room_hall/room_hall.js
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify';
import Toast  from '../../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../../miniprogram_npm/@vant/weapp/dialog/dialog';
import { GET, POST } from '../../../utils/https';

Page({
    data: {
        pages_info:0,
        querys:{
            room_search_param : '',
            room_max : '',
            room_status : 1,
            pageIndex : 1,
            pageSize : 20
        },
        select_all_list: false,
        option: [
            { text: '所有游戏房间', value: "" },
            { text: '3人房间', value: 3 },
            { text: '4人房间', value: 4 },
            { text: '8人房间', value: 8 },
            { text: '9人房间', value: 9 },
            { text: '12人房间', value: 12 },
        ],
        room_list: [],
        room_count:"",
        activeNames:"",         //房间列表手风琴
        network_status: true,	//获取房间列表是否成功【网络状态】
        network_status:"false", 
        _freshing:true,
        triggered: false,       //微信小程序是否触发下啦刷新
        finished:false,         //是否加载完成，显示【没有啦】字体
    },
    intoRoom:function(e) {   
        //console.log(e.currentTarget.dataset.item); 
        Dialog.confirm({ title: e.currentTarget.dataset.item.room_title, message: '确认进入该房间吗？' }).then(() => {
            if (e.currentTarget.dataset.item.current >= e.currentTarget.dataset.item.room_max) {
                Toast({ message: "房间内人数已满，无法加入游戏", position: 'bottom' ,zIndex:5000 });return false;
            }
            //发送请求，验证数据库中房间状态
            POST('/game/hall/into_room', e.currentTarget.dataset.item).then(res => {
                if (res.code == 200 ){
                    if (e.currentTarget.dataset.item.current >= e.currentTarget.dataset.item.room_max) {
                        Toast({ message: "房间内人数已满，无法加入游戏", position: 'bottom' ,zIndex:5000 });return false;
                    } else {
                        //拼接跳转参数 params ，然后跳转
                        let route_query = '?id='+e.currentTarget.dataset.item.id+'&room_number='+e.currentTarget.dataset.item.room_number;
                        //wx.redirectTo({ url: '../room_info/room_info'+route_query })
                        wx.navigateTo({
                            url: '../room_info/room_info'+route_query
                        })
                    }
                } else {
                    Toast({ message: res.msg, duration: 1500 });return false;
                }
            }).catch((e) => {
                console.log(e);
                Notify({ type: 'danger', message: '连接服务器失败' });return false;
            });
        }).catch(() => {
            return false;
        });
    },
    onReady: function () {
        //隐藏主页按钮
        wx.hideHomeButton();
    },
    onReachBottom(){
        //触底加载数据
        this.onLoad();
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getTabBar().setData({ ['active']: 0 });
    },
    onLoad: function () {
        //为了避免重复请求，要先判断是否加载完成，finished 也是显示【没有啦】字体的关键字段
        if ( this.data.finished == true ) {
            //轻量级提示
            this.setData({ ['finished'] : true });return;
        }

        //发送请求，页数增加
        POST('/game/hall/room_list', this.data.querys).then(res => {
            if (res.code == 200){
                this.setData({ ['network_status'] : true })		//设置网络状态

                //拼接数据
                var res_list = this.data.room_list;
                for(let i in res.data.data){
                    res_list.push(res.data.data[i]);
                }
                this.setData({ ['room_list'] : res_list });     //房间列表
                this.setData({ ['room_count'] : res.data.count });  //房间总数

                //设置下一次页码
                this.changeDimensional('pageIndex',this.data.querys.pageIndex+1);
                //判断是否还有下一页
                if (this.data.room_list.length >= this.data.room_count ) {
                    this.setData({ ['finished'] : true });
                } 
            } else {
                Notify({ type: 'danger', message: '获取房间列表失败' });
                this.data.network_status = false;	//设置网络状态
            }
        }).catch((e) => {
            this.setData({ ['network_status'] : false })		//设置网络状态
            console.log(e);
            Notify({ type: 'danger', message: '连接服务器失败' });
        });
    },
    onChange(event) {
        this.setData({ activeNames: event.detail });
    },
    changeRoomMax:function(e){
        this.changeDimensional('room_max',e.detail);
        // 清空列表数据
        this.setData({ ['room_list'] : [] });
        this.setData({ ['room_count'] : '' });
        this.setData({ ['finished'] : false });
        //设置页码 
        this.changeDimensional('pageIndex',1);
        //重新加载
        this.onLoad();
    },
    changeRoomStatus:function(e) {
        this.setData({ ['select_all_list'] : e.detail });
        //剩下的交给onConfirm 负责
    },
    onConfirm() {
        if (this.data.select_all_list == true) {
            this.changeDimensional('room_status',"");
        } else {
            this.changeDimensional('room_status',1);
        }
        
        // 清空列表数据
        this.setData({ ['room_list'] : [] });
        this.setData({ ['room_count'] : '' });
        this.setData({ ['finished'] : false });
        //设置页码 
        this.changeDimensional('pageIndex',1);
        //这句话是关闭下拉菜单的
        this.selectComponent('#item').toggle();
        //重新加载
        this.onLoad();
    },
    SearchInput:function(e){
        this.changeDimensional('room_search_param',e.detail);
        //剩下的交给 onSearch 负责
    },
    onSearch() {
        // 清空列表数据
        this.setData({ ['room_list'] : [] });
        this.setData({ ['room_count'] : '' });
        this.setData({ ['finished'] : false });
        //设置页码 
        this.changeDimensional('pageIndex',1);
        //重新加载
        this.onLoad();
    },
    changeDimensional(key,value){
        //设置下一次页码
        let querys = this.data.querys;
        querys[key] = value;
        this.setData({ ['querys'] : querys });
        console.log(this.data.querys)
    },
})