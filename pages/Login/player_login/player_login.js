// pages/Login/player_login.js
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify';
import Toast  from '../../../miniprogram_npm/@vant/weapp/toast/toast';
import { GET, POST } from '../../../utils/https';

const app = getApp()
Page({
    data: {
        // 组件所需的参数
    navbarData: {
        showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
        title: '我的主页', //导航栏 中间的标题
      },
  
      // 此页面 页面内容距最顶部的距离
      height: app.globalData.height * 2 + 20 ,  
        params:{
            telphone:"17611325509",
            password:"jiannei0507"
        },
        errmsg:{
            telphone:"",
            password:""
        },
        forgetDoing:false,
        forgetDoingFriends:"480618240",
        forgetDoingEnvelop:"ausheng@foxmail.com",
    },
    playerLogin(){
        // Notify({ type: 'success', message: '登录成功，欢迎回来',safeAreaInsetTop:true });
        // return;
        if(!this.ValidationInput()){
            Toast({ message: "请检查您的表格参数", position: 'bottom' });
            return false;
        } else {
            POST('/game/player/login',this.data.params).then(res => {
                if (res.code == 200) {
                    Notify({ type: 'success', message: '登录成功，欢迎回来' });
                    
                    wx.setStorageSync('player_token', res.data._token);

                    //console.log(wx.getStorageSync('player_token'));return;
                    delete res.data['_token'];
                    wx.setStorageSync('player_info', encodeURIComponent(JSON.stringify(res.data)));
                    //console.log(decodeURIComponent(wx.getStorageSync('player_info')));return;
					
					wx.switchTab({ url: '../../Game/room_hall/room_hall' })
				} else {
					Toast({ message: res.msg, position: 'bottom',duration: 2000 });
				}
            }).catch((e) =>{
                console.log(e)
            })
        }
    },
    telphoneInput:function(e){
        var myreg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['telphone']:"请输入正确的电话号码"}})
            return false;
        } else {
            this.setData({['errmsg']:{['telphone']:""}})
            this.data.params.telphone = e.detail;
            return true;
        }
    },
    passwordInput:function(e){
        var myreg = /^[_a-zA-Z0-9]{6,18}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['password']:"请检查您的密码，长度保持在6-18位"}})
            return false;
        } else {
            this.setData({['errmsg']:{['password']:""}})
            this.data.params.password = e.detail;
            return true;
        }
    },
    ValidationInput(){
        if (!this.telphoneInput({'detail':this.data.params.telphone})) {
            return false;
        } else if(!this.passwordInput({'detail':this.data.params.password})){
            return false;
        } else {
            return true;
        }
    },
    goRegister(){
        // wx.redirectTo({ url: '../player_register/player_register' })
        wx.navigateTo({
            url: '../player_register/player_register',
        })
    },
    showForgetDoing(){
        this.setData({ forgetDoing : true })
    },
    copyFriends:function(e){
        wx.hideToast()
        wx.setClipboardData({ data: this.data.forgetDoingFriends,
            success: function (res) {
                wx.hideToast()
                Toast({ message: "已经复制到您的剪切板", position: 'bottom' ,zIndex:5000 });
            },
            fail: function (res) { wx.hideToast() },
            complete:function(res) { wx.hideToast() }
        })
    },
    copyEnvelop:function(e){
        wx.hideToast()
        wx.setClipboardData({ data: this.data.forgetDoingEnvelop,
            success: function (res) {
                wx.hideToast()
                Toast({ message: "已经复制到您的剪切板", position: 'bottom' ,zIndex:5000 });
            },
            fail: function (res) { wx.hideToast() },
            complete:function(res) { wx.hideToast() }
        })
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        //console.log('版本：', __wxConfig);
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {},

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {},

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {},

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {},

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {}
})