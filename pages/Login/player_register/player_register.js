// pages/Login/player_login.js
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'

Page({
    data: {
        params:{
            nickname:"",
            telphone:"",
            tel_code:"",
            password:""
        },
        errmsg:{
            nickname:"",
            telphone:"",
            tel_code:"",
            password:""
        },
        sendCodeButton:true,
        button_content:"发送验证码",
    },
    onSubmit(){
        if(!this.ValidationInput()){
            Toast({ message: "请检查您的表格参数", position: 'bottom' });
        } else {
            console.log("register")
        }
    },
    nicknameInput:function(e){
        var myreg = /^[\u4e00-\u9fa5a-zA-Z]{2,5}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['nickname']:"仅支持中文或者中文，2-5个字符"}})
            return false;
        } else {
            this.setData({['errmsg']:{['nickname']:""}})
            this.data.params.nickname = e.detail;
            return true;
        }
    },
    telphoneInput:function(e){
        var myreg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['telphone']:"请输入正确的电话号码"}})
            return false;
        } else {
            this.setData({['errmsg']:{['telphone']:""}})
            this.data.params.telphone = e.detail;
            return true;
        }
    },
    telCodeInput:function(e){
        var myreg = /^\d{6}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['tel_code']:"请输入六位数字的验证码"}})
            return false;
        } else {
            this.setData({['errmsg']:{['tel_code']:""}})
            this.data.params.tel_code = e.detail;
            return true;
        }
    },
    passwordInput:function(e){
        var myreg = /^[_a-zA-Z0-9]{6,18}$/;

        if (!myreg.test(e.detail)) {
            this.setData({['errmsg']:{['password']:"请检查您的密码，长度保持在6-18位"}})
            return false;
        } else {
            this.setData({['errmsg']:{['password']:""}})
            this.data.params.password = e.detail;
            return true;
        }
    },
    ValidationInput(){
        if (!this.nicknameInput({'detail':this.data.params.nickname})) {
            return false;
        } else if (!this.telphoneInput({'detail':this.data.params.telphone})) {
            return false;
        } else if(!this.telCodeInput({'detail':this.data.params.tel_code})){
            return false;
        } else if(!this.passwordInput({'detail':this.data.params.password})){
            return false;
        } else {
            return true;
        }
    },
    send_tel_code(){
        // 校验手机号
        if (!this.telphoneInput({'detail':this.data.params.telphone})) {
            Toast({ message: "请输入正确的电话号码", position: 'bottom' });
            return false;
        }
        
        let count_time = 60;
        let this_count = 60;
        let timer = '';
        timer = setInterval(() => {
            if (this_count > 0 && this_count <= count_time){
                // 倒计时时不可点击 - 计时秒数 - 更新按钮的文字内容
                this.setData({"sendCodeButton":false})
                this_count--;
                this.setData({"button_content":this_count+'s后重新获取'})
            } else {
                // 倒计时完，可点击 - 更新按钮文字内容 - 清空定时器
                this.setData({"sendCodeButton":true})
                this.button_content = '发送验证码';
                
                clearInterval(timer);
                timer = null;
            }
        }, 1000);
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {},

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {},

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {},

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {},

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {}
})